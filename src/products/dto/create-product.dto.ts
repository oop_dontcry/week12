import { IsNotEmpty, MaxLength } from 'class-validator';
export class CreateProductDto {
  @IsNotEmpty()
  @MaxLength(50)
  name: string;
  @IsNotEmpty()
  price: number;
}
