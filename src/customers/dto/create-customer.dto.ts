import { IsNotEmpty, MinLength, MaxLength } from 'class-validator';
export class CreateCustomerDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  age: number;

  @IsNotEmpty()
  @MinLength(10)
  @MaxLength(10)
  tel: string;
  @IsNotEmpty()
  gender: string;
}
